/* Author: Dylan Kozicki
 * Created: November 17, 2015
 * 
 * This file is part of PiKVM.
 *
 * PiKVM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * PiKVM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the
 * GNU General Public License along with PiKVM.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <PluggableUSB.h>
#include <Wire.h>
#define SLAVE_ADDRESS 0x04
#include "HID-Project.h"

int number = 0;
int state = 0;
const int pinLed = LED_BUILTIN;
const int pinButton = 2;
char input;
void setup() {
    pinMode(pinLed, OUTPUT);


    Serial.begin(9600); // start serial for output
    Wire.begin(SLAVE_ADDRESS);
    // define callbacks for i2c communication
    Wire.onReceive(receiveData);
    Wire.onRequest(sendData);

    // Sends a clean report to the host. This is important on any Arduino type.
    BootKeyboard.begin();
}
void loop() {
    delay(100);
}

// callback for received data
void receiveData(int byteCount){

    while(Wire.available()) {
        number = Wire.read();
        Serial.print("Data recieved");
        Serial.println(number);

        BootKeyboard.write(number);

    }
}


// callback for sending data
void sendData(){
    Wire.write(number);
}
