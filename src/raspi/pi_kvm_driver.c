/* Author: Dylan Kozicki
 * Created: November 17,2015
 *
 * This file is part of PiKVM.
 *
 * PiKVM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * PiKVM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY{} without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the
 * GNU General Public License along with PiKVM.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "pi_kvm_driver.h"
#include "arduino_kb.h"
#include <wiringPi.h>
#include <unistd.h>

//***************************************************************
//* name : kvm_init
//* description : initializes the kvm driver with
//*               the given i2c device address
//* input : devID - the address of the i2c device
//* output: 0 for success -1 otherwise
//***************************************************************
int kvm_init(int devID){

    keyboard_init(I2C_ADDRESS); //initialize the keyboard
    wiringPiSetupGpio(); //initialize the gpio

    //set pin modes
    pinMode(PWR_BTN_PIN, OUTPUT);
    pinMode(RES_BTN_PIN, OUTPUT);
    pinMode(CMOS_RES_PIN, OUTPUT);
    pinMode(ARDUINO_RESET_PIN, OUTPUT);
    pinMode(PWR_LED_INPUT_PIN, INPUT);


}

//***************************************************************
//* name : enter_bios
//* description : sends commands required to boot into bios
//* input : mode - delimites what kind of shutdown is needed
//* output: 0 for success -1 otherwise
//***************************************************************
int enter_bios(int mode){
    int i;
    switch(mode){
        case MODE_SAFE:
            //insert safe code later
        break;
        case MODE_FORCE:
            reset_computer();
           break;

    }
    for(i = 0; i< BIOS_KEY_NUM_PRESS; i++){
         keyboard_write(KEY_ENTER_BIOS);
         sleep(KEY_PRESS_WAIT);
    }

}

//***************************************************************
//* name : reset_arduino
//* description : resets the arduino
//* input : N/A
//* output: 0 for success -1 otherwise
//***************************************************************
int reset_arduino(){
    digitalWrite(ARDUINO_RESET_PIN, HIGH);
    sleep(BTN_PRESS_WAIT);
    digitalWrite(ARDUINO_RESET_PIN, LOW);
}

//***************************************************************
//* name : reset_computer
//* description : resets the computer
//* input : N/A
//* output: 0 for success -1 otherwise
//***************************************************************
int reset_computer(){
    digitalWrite(RES_BTN_PIN, HIGH);
    sleep(BTN_PRESS_WAIT);
    digitalWrite(RES_BTN_PIN, LOW);
}

//***************************************************************
//* name : power_on_computer
//* description : turns the computer on
//* input : N/A
//* output: 0 for success -1 otherwise
//***************************************************************
int power_on_computer(){
    digitalWrite(PWR_BTN_PIN, HIGH);
    sleep(BTN_PRESS_WAIT);
    digitalWrite(PWR_BTN_PIN, LOW);

}

//***************************************************************
//* name : clear_cmos
//* description : clears the cmos of the computer
//* input : N/A
//* output: 0 for success -1 otherwise
//***************************************************************
int clear_cmos(){
     digitalWrite(CMOS_RES_PIN, HIGH);
     if(get_power_status == STATUS_ON ){
     reset_computer();

}
else{
    power_on_computer();
}
sleep(BTN_PRESS_WAIT);
digitalWrite(CMOS_RES_PIN, LOW);
sleep(BTN_PRESS_WAIT);
reset_computer();
}


//***************************************************************
//* name : get_power_status
//* description : determine what state the computer is in
//* input : N/A
//* output: 0 for success -1 otherwise
//***************************************************************
int get_power_status(){
     int status = digitalRead(PWR_LED_INPUT_PIN)
    sleep(BTN_PRESS_WAIT);
     status = digitalRead(PWR_LED_INPUT_PIN);
     return status;
}

//***************************************************************
//* name : enter_boot_selection
//* description : enters the boot selection menu
//* input : mode - delimites what kind of shutdown is needed
//* output: 0 for success -1 otherwise
//***************************************************************
int enter_boot_selection(){

    int i;
    switch(mode){
        case MODE_SAFE:
            //insert safe code later
        break;
        case MODE_FORCE:
            reset_computer();
           break;

    }
    for(i = 0; i< BIOS_KEY_NUM_PRESS; i++){

         keyboard_write(KEY_BOOT_SELECTION);
         sleep(KEY_PRESS_WAIT);
    }
}





