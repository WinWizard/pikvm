/* Author: Dylan Kozicki
 * Created: November 17, 2015
 *
 * This file is part of PiKVM.
 *
 * PiKVM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * PiKVM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the
 * GNU General Public License along with PiKVM.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 */



#include "arduino_kb.h"
#include <wiringPiI2C.h>

//***************************************************************
//* name : keyboard_init
//* description : initializes the keyboard with
//*               the given i2c device address
//* input : devID - the address of the i2c device
//* output: 0 for success -1 otherwise
//***************************************************************
int keyboard_init(int devID){
    device = wiringPiI2CSetup(devID);

}
//***************************************************************
//* name : kvm_write
//* description : writes the given key to the keyboard
//* input : key - the key to be written
//* output: 0 for success -1 otherwise
//***************************************************************
int keyboard_write(int key){
    wiringPiI2CWrite(device,key);
}
