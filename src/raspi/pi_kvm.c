
/* Author: Dylan Kozicki
 * Created: November 17, 2015
 *
 * This file is part of PiKVM.
 *
 * PiKVM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * PiKVM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the
 * GNU General Public License along with PiKVM.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "arduino_kb.h"
#include "pi_kvm_driver.h"
#define I2C_ADDRESS 0x04


int main(){

    kvm_init(I2C_ADDRESS);
    return 0;
}
