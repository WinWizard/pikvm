/* Author: Dylan Kozicki
 * Created: November 17, 2015
 *
 * This file is part of PiKVM.
 *
 * PiKVM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * PiKVM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the
 * GNU General Public License along with PiKVM.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef PI_KVM_DRIVER_INCLUDED
#define PI_KVM_DRIVER_INCLUDED

//constants for bios entry
#define KEY_ENTER_BIOS KEY_DELETE
#define BIOS_KEY_NUM_PRESSES 20
//constants for boot_selection
#define KEY_BOOT_SELECTION KEY_F12

//constants for pin mapping
#define PWR_BTN_PIN 29
#define RES_BTN_PIN 28
#define CMOS_RES_PIN 27
#define PWR_LED_INPUT_PIN 26
#define ARDUINO_RESET_PIN 25

#define MODE_SAFE 0
#define MODE_FORCE 1

#define KEY_PRESS_WAIT 250
#define BTN_PRESS_WAIT 100

#define STATUS_ON 1
#define STATUS_OFF 0



int kvm_init(int devID);
//initializes kvm driver
int enter_bios(int mode);
//sends commands to enter the bios
int reset_arduino();
//resets the arduino
int reset_computer();
//resets the computer
int power_on_computer();
//turns the computer on
int clear_cmos();
//clears the cmos
int get_power_status();
//returns the status of the pc
int enter_boot_selection(int mode);
//enters the boot selection menu












#endif

